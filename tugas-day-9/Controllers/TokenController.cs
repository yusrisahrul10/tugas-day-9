﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Security.Claims;
using System.Xml.Linq;
using tugas_day_9.Logics;

namespace tugas_day_9.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TokenController : ControllerBase
    {
        [HttpGet]
        [Route("GenerateToken")]
        public ActionResult GenerateToken()
        {
            string tokenJwt = JwtTokenLogic.GenerateJwtToken(new[]
             {
                    new Claim("name", "yusri"),
                    new Claim(ClaimTypes.Role, "admin")
             }
           );

            return Ok(tokenJwt);
        }

        [HttpGet]
        [Route("ValidateToken")]
        [Authorize]
        public ActionResult ValidateToken()
        {
            try
            {
                return StatusCode(200, "Success");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }
    }
}
